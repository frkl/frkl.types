#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_types` package."""
from abc import ABCMeta

import pytest  # noqa
from frkl.types.typistry import Typistry


def test_typistry():
    class AClass(metaclass=ABCMeta):

        pass

    class Class1(AClass):

        pass

    class Class2(AClass):

        pass

    typistry = Typistry()

    pm = typistry.get_plugin_manager(AClass)
    assert list(pm.plugin_names) == ["class1", "class2"]

    # TODO: more tests for this

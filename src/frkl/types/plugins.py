# -*- coding: utf-8 -*-
from enum import Enum
from typing import Any, Dict, Iterable, List, Mapping, Type, Union

from frkl.common.dicts import get_seeded_dict
from frkl.common.doc import Doc
from frkl.common.exceptions import FrklException
from frkl.common.iterables import ensure_iterable
from frkl.common.types import get_type_alias


class PLUGIN_TYPE(Enum):

    singleton = 0
    instance = 1


def get_plugin_name(plugin: Type, name_attribute: str = "_plugin_name"):

    if hasattr(plugin, name_attribute):
        plugin_name = getattr(plugin, name_attribute)
    else:
        plugin_name = get_type_alias(plugin, include_module_path=False)

    plugin_name = plugin_name.replace("-", "_")
    return plugin_name


class PluginManager(object):
    def __init__(
        self,
        base_class: Type,
        *plugins: Type,
        _name_attr: str = "_plugin_name",
        _supports_attr: str = "_plugin_supports",
    ):

        self._base_class: Type = base_class
        self._plugins: Dict[str, Type] = {}
        self._supports_map: Dict[Union[str, Type], Type] = {}
        # self._config: Mapping[str, Any] = config

        self._name_attribute: str = _name_attr
        self._supports_attribute: str = _supports_attr

        self._plugin_sets: Dict[str, PluginManager] = {}

        for plugin in plugins:
            self._add_plugin(plugin)

    @property
    def base_class(self) -> Type:
        return self._base_class

    @property
    def plugin_names(self) -> Iterable[str]:

        return self._plugins.keys()

    def _add_plugin(self, plugin: Type):

        try:
            plugin_name = get_plugin_name(
                plugin=plugin, name_attribute=self._name_attribute
            )

            self._plugins[plugin_name] = plugin

            if hasattr(plugin, self._supports_attribute):
                supports = getattr(plugin, self._supports_attribute)
                supports_list: List[Union[str, Type]] = ensure_iterable(supports, always_create_new_obj=True)  # type: ignore
            else:
                supports_list = []
            supports_list.append(plugin_name)
            supports_list.append(plugin)

            for s in supports_list:
                if isinstance(s, str):
                    _s = s.replace("-", "_").lower()
                    self._supports_map[_s] = plugin
                elif isinstance(s, type):
                    self._supports_map[s] = plugin
                else:
                    raise TypeError(
                        f"Invalid type for '_plugin_supports' attribute: {type(s)}"
                    )
        except Exception as e:
            raise FrklException(msg=f"Can't add plugin '{plugin.__name__}'", parent=e)

    def get_plugin_doc(self, plugin_name: str) -> Doc:

        plugin_class = self.get_plugin(plugin_name=plugin_name, raise_exception=True)
        doc = Doc.from_docstring(plugin_class)
        return doc

    def get_plugin(self, plugin_name: str, raise_exception: bool = False) -> Any:

        plugin_name = plugin_name.replace("-", "_")

        result = self._plugins.get(plugin_name, None)
        if result is None and raise_exception:
            if self.plugin_names:
                avail = ", ".join(self.plugin_names)
            else:
                avail = "none"
            raise FrklException(
                msg=f"Can't retieve plugin '{plugin_name}' for '{self.base_class.__name__}'.",
                reason=f"No such plugin registered, available plugins: {avail}",
                solution="Make sure all modules that include plugin classes are loaded, and all required plugins implement all abstract methods.",
            )

        return result

    def get_plugin_for(
        self, item_type: Union[Type, str], raise_exception: bool = False
    ):

        _item_type = item_type.replace("-", "_").lower()
        result = self._supports_map.get(_item_type, None)
        if result is None and raise_exception:
            raise FrklException(
                msg=f"Can't retrieve plugin for '{item_type}' amongst plugins for '{self.base_class.__name__}'.",
                reason="No suitable plugin registered.",
            )

        return result


class PluginFactory(object):
    def __init__(
        self,
        plugin_manager: PluginManager,
        singleton: bool = False,
        allow_config_override: bool = False,
        **default_plugin_config: Any,
    ):

        self._plugin_manager: PluginManager = plugin_manager
        self._default_plugin_config: Mapping[str, Any] = default_plugin_config
        self._allow_config_override: bool = allow_config_override

        self._is_singleton: bool = singleton
        self._singletons: Dict[str, object] = {}

    @property
    def plugin_names(self):

        return self._plugin_manager.plugin_names

    @property
    def base_class(self) -> type:
        return self._plugin_manager.base_class

    @property
    def plugin_type_map(self) -> Mapping[str, Type]:

        return self._plugin_manager._plugins

    def get_plugin_doc(self, plugin_name) -> Doc:

        return self._plugin_manager.get_plugin_doc(plugin_name)

    def create_plugin(
        self, plugin_name: str, raise_exception: bool = True, **config: Any
    ) -> Any:

        if self._is_singleton and plugin_name in self._singletons.keys():
            raise FrklException(
                msg=f"Can't create plugin of type '{self._plugin_manager.base_class.__name__}'.",
                reason=f"Factory is configured to create singletons, but plugin for '{plugin_name}' already created.",
            )

        if self._is_singleton and not self._allow_config_override and config:
            raise FrklException(
                msg=f"Can't create plugin of type '{self._plugin_manager.base_class.__name__}'.",
                reason="Config provided, but overriding the base config is not allowed for this factory.",
            )

        plugin_cls = self._plugin_manager.get_plugin(
            plugin_name, raise_exception=raise_exception
        )
        _config = get_seeded_dict(self._default_plugin_config, config)

        try:
            plugin_obj = plugin_cls(**_config)
        except Exception as e:
            raise FrklException(
                msg=f"Can't create plugin for type '{self._plugin_manager.base_class.__name__}'.",
                reason=f"Provided config invalid: {_config}",
                parent=e,
            )

        if self._is_singleton:
            self._singletons[plugin_name] = plugin_obj

        return plugin_obj

    def get_singleton(self, plugin_name: str, raise_exception: bool = True) -> Any:

        if not self._is_singleton:
            raise FrklException(
                msg=f"Can't retrieve singleton plugin '{plugin_name}' of plugin type '{self.base_class.__name__}'.",
                reason="This factory does not manage singletons, but instances.",
            )
        if plugin_name not in self._singletons:
            self.create_plugin(plugin_name, raise_exception=raise_exception)
        return self._singletons[plugin_name]

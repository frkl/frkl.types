# -*- coding: utf-8 -*-
from typing import Any, Dict


project_name = "frkl.types"
project_main_module = "frkl.types"
project_slug = "frkl_types"

pyinstaller: Dict[str, Any] = {}

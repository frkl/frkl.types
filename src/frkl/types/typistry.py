# -*- coding: utf-8 -*-
import inspect
import logging
from fnmatch import fnmatch
from types import ModuleType
from typing import Any, Dict, Iterable, List, Optional, Type, Union

from frkl.common.exceptions import FrklException
from frkl.common.types import (
    get_all_subclasses,
    get_class_from_string,
    get_type_alias,
    load_modules,
)
from frkl.types.plugins import PluginFactory, PluginManager


log = logging.getLogger("frkl")


def ensure_type(cls: Union[str, Type]) -> Type:
    """Ensure a provided argument is a Type object.

    If a string, the class will be loaded and returned.
    """

    if isinstance(cls, type):
        return cls

    else:
        return get_class_from_string(cls)


class TypistryException(FrklException):
    """Frkl exception that holds a typistry object."""

    def __init__(self, *args, **kwargs):

        self._typistry = kwargs.pop("typistry")
        super().__init__(*args, **kwargs)

    @property
    def typistry(self) -> "Typistry":
        return self._typistry


class Typistry(object):
    """Class to manage loaded modules and types.

    The main reason for this class to exist is to provide a 'PluginManager' object, and to assure that all the class/module loading has been taken care of
    for it.

    This class is a bit more complex than it strictly needed to be. Mostly
    in the interest of performance, as it loads modules/classes lazy, to
    avoid a speed penalty in case an application is only called for minor
    tasks ('--help', etc.).

    TODO: more documentation

    Args:
      modules (Optional[Iterable[str]]): a list of modules to pre-load before trying to access any classes
      classes (Optional[Iterable[Type]]): a list of classes to be managed
    """

    def __init__(self, *args, **kwargs) -> None:

        self._modules: List[str] = []
        self._classes: List[Type] = []
        self._registered: Dict[Type, Dict] = {}
        self._invalidated: bool = False
        self._aliases: Dict[str, Type] = {}
        self._ids: Dict[Type, str] = {}
        self._id_strategy: str = "alias"

        self._registered_plugin_factories: Dict[str, "PluginFactory"] = {}

        modules: List[str] = kwargs.get("modules", None)
        if modules:
            self.add_module_paths(*modules)

        classes = kwargs.get("classes", None)
        if classes:
            self.add_classes(*classes)

    def add_classes(self, *classes):
        """Add classes (and their modules) to this Typistry."""

        if not classes:
            return

        to_add = []
        for cls in classes:
            c = ensure_type(cls)
            to_add.append(c)
        self.add_module_paths(*[cls.__module__ for cls in to_add])
        self.register_classes(*to_add)

    def _invalidate(self) -> None:
        """Invalidate the state of this Typistry.

        This means a new module has been added, but not loaded yet.
        """

        self._invalidated = True  # type: ignore

    @property
    def invalidated(self) -> bool:
        """Whether this Typistry's state is valid (all modules loaded), or not."""

        return self._invalidated  # type: ignore

    def add_module_paths(self, *module_paths: str) -> None:
        """Add a module path for this Typistry to manage."""

        if not module_paths:
            return

        m: List[ModuleType] = load_modules(module_paths)
        changed = False
        for module in m:
            if module.__name__ not in self.modules:
                changed = True
                self.modules.append(module.__name__)

        if changed:
            self._invalidate()

    @property
    def modules(self) -> List[str]:
        """Return a list of module paths managed by this Typistry."""

        return self._modules  # type: ignore

    @property
    def base_classes(self) -> Iterable[Type]:
        """Return a list of classes registered in this Typistry."""

        return self.registered_classes.keys()

    @property
    def aliases(self) -> Dict[str, Type]:
        """Return a map of registered class aliases and their classes.

        This is used internally to be able to refer to classes with strings,
        and make users lifes easier.
        """

        return self._aliases

    def get_id(self, cls: Type) -> Optional[str]:
        """Get the internal id of a registered class."""

        return self._ids.get(cls, None)

    @property
    def registered_classes(self) -> Dict[Type, Dict]:
        """Return a map of registered classes, and their metadata."""

        return self._registered  # type: ignore

    def check_allowed(self, cls: Union[Type, str], raise_exception=True) -> bool:
        """Check whether the specified class is allowed in this context.

        Currently, this means whether it is under any of the modules in the 'modules' list attribute.

        Args:
            cls (Union[Type, str]): the type to check
        """

        if isinstance(cls, str) and cls in self.aliases.keys():
            _cls = self.aliases[cls]
        else:
            _cls = ensure_type(cls)
        path = _cls.__module__

        for m in self.modules:
            if fnmatch(path, m):
                return True

            if m.endswith(".*"):
                if m[0:-2] == path:
                    return True

        if raise_exception:
            if not self.modules:
                raise TypistryException(
                    typistry=self,
                    msg=f"Type '{cls}' not allowed as member of this Typistry.",
                    reason="No python modules paths added yet to this Typestry.",
                )
            else:
                raise TypistryException(
                    typistry=self,
                    msg=f"Type '{cls}' not allowed as member of this Typistry.",
                    reason=f"Not part of any of the allowed modules: {', '.join(self.modules)}",
                )
        return False

    def _add_to_aliases(self, cls: Type) -> None:
        """Add an auto generated alias for the specified class to the internal alias map."""

        cls_name = cls.__name__
        cls_name_full = f"{cls.__module__}.{cls.__name__}"
        cls_alias = get_type_alias(cls)
        cls_alias_name = cls_alias.split(".")[-1]

        if cls_name in self.aliases.keys() or cls_alias_name in self.aliases.keys():
            return

        self.aliases[cls_name] = cls
        self.aliases[cls_name_full] = cls
        self.aliases[cls_alias] = cls
        self.aliases[cls_alias_name] = cls

        if self._id_strategy == "alias":
            self._ids[cls] = cls_alias_name

    def is_registered(self, cls: Union[str, Type]) -> bool:
        """Check whether a class (or it's alias) is registered with this Typistry."""

        if isinstance(cls, str):
            return cls in self.aliases.keys()
        else:
            return cls in self.registered_classes.keys()

    def get_class(self, cls: Union[str, Type]) -> Type:
        """Return a class for the specified input.

        If a class is provided, it will be registered before being returned.
        If a string is provided, it will be used to load the class it is
        refereing to.

        Args:
            cls (Union[str, Type]): the class
        Returns:
            Type: the (now ensured to be registered) class
        """

        if isinstance(cls, str):
            val = self.aliases.get(cls, None)
            if val is not None:
                return val

            _cls = ensure_type(cls)
            self.register_classes(_cls)
            return _cls
        else:
            self.register_classes(cls)
            return cls

    def register_classes(self, *classes: Union[str, Type]) -> None:
        """Registere one or several classes in this Typistry."""

        for _cls in classes:
            if isinstance(_cls, str):
                cls = ensure_type(_cls)
            else:
                cls = _cls
                self.add_module_paths(cls.__module__)

            registered = self.registered_classes.get(cls, None)
            if registered is None:
                self.check_allowed(cls)
                self.registered_classes.setdefault(cls, {})
                self._add_to_aliases(cls)

    def get_subclasses(self, base_class: Union[Type, str]) -> List[Type]:
        """Return all (non-abstract) subclasses of a base class.

        The base class must pass the 'check_allowed' check.
        """

        if not self.is_registered(base_class):
            self.register_classes(base_class)

        _base_class = self.get_class(base_class)
        cls_dict = self.registered_classes[_base_class]

        if "subclasses" in cls_dict.keys():
            return cls_dict["subclasses"]

        # subclasses_list = cls_dict.setdefault("subclasses", [])
        # if not inspect.isabstract(_base_class):
        #     subclasses_list.append(_base_class)

        all_subclasses = get_all_subclasses(_base_class)

        self.registered_classes[_base_class].setdefault("subclasses", [])
        for sc in all_subclasses:
            if inspect.isabstract(sc):

                log.debug(f"Ignoring subclass '{sc}': is abstract")
                continue
            log.debug(f"Adding subclass: {sc}")
            if sc in self.registered_classes[_base_class]["subclasses"]:
                continue

            self._add_to_aliases(sc)
            self.registered_classes[_base_class]["subclasses"].append(sc)

        return self.registered_classes[_base_class]["subclasses"]

    def get_plugin_manager(self, base_class: Union[Type, str],) -> PluginManager:
        """Return a PluginManager object loaded with the current state of
        configured modules and classes.

        If a module/class is registered after this PluginManager is created,
        the PluginManager won't know about them.
        """

        if not self.is_registered(base_class):
            self.register_classes(base_class)

        _base_class = self.get_class(base_class)
        cls_dict = self.registered_classes[_base_class]

        if cls_dict.get("plugin_manager", None) is not None:
            return cls_dict["plugin_manager"]

        subclasses = self.get_subclasses(base_class=_base_class)
        plugin_manager = PluginManager(_base_class, *subclasses)
        cls_dict["plugin_manager"] = plugin_manager
        return plugin_manager

    def register_plugin_factory(
        self,
        factory_name: str,
        base_class: Type,
        singleton: bool = False,
        allow_config_override: bool = False,
        use_existing: bool = False,
        **default_plugin_config: Any,
    ) -> "PluginFactory":

        existing = self._registered_plugin_factories.get(factory_name, None)
        if existing:
            if use_existing:
                # TODO: check if base classes are same?
                return existing
            else:
                raise FrklException(
                    msg=f"Can't register plugin factory '{factory_name}'.",
                    reason="Factory with that name already registered.",
                )

        plugin_manager = self.get_plugin_manager(base_class)
        factory = PluginFactory(
            plugin_manager=plugin_manager,
            singleton=singleton,
            allow_config_override=allow_config_override,
            **default_plugin_config,
        )
        self._registered_plugin_factories[factory_name] = factory
        return factory

    def get_plugin_factory(
        self, factory_name: str, raise_exception: bool = True
    ) -> Optional[PluginFactory]:

        f = self._registered_plugin_factories.get(factory_name, None)
        if raise_exception and f is None:
            raise FrklException(
                msg=f"Can't get plugin factory '{factory_name}'.",
                reason="No factory with that name registered.",
            )
        return f

# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


frkl_types_app_dirs = AppDirs("frkl_types", "frkl")

if not hasattr(sys, "frozen"):
    FRKL_TYPES_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frkl_types` module."""
else:
    FRKL_TYPES_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "frkl_types"  # type: ignore
    )
    """Marker to indicate the base folder for the `frkl_types` module."""

FRKL_TYPES_RESOURCES_FOLDER = os.path.join(FRKL_TYPES_MODULE_BASE_FOLDER, "resources")
DEFAULT_PLUGIN_MANAGER_CONFIG = {
    "supports_attribute": "_plugin_supports",
    "name_attribute": "_plugin_name",
}
